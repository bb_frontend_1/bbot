import { useEffect, useState } from "react";
import ApiPoints from "./ApiPoints";

export default function useRequest( point, method, data ) {
  const [response, setResponse] = useState({});

  useEffect(() => {
    let request;
    let headers;
    let options;
    switch (method) {
      case "GET":
        headers = new Headers();
        options = { method, headers };
        request = new Request(ApiPoints[point], options);
        break;
      case "POST":
        headers = new Headers({ "Content-Type": "application/json" });
        options = { method, body: JSON.stringify(data), headers };
        request = new Request(ApiPoints[point], options);
        break;
      case "PUT":
        headers = new Headers({ "Content-Type": "application/json" });
        options = { method, body: JSON.stringify(data), headers };
        request = new Request(ApiPoints[point], options);
        break;
      default:
        break;
    }
    fetch(request)
      .then((res) => {
        return res.json();
      })
      .then((data) => setResponse(data))
      .catch((err) => console.error);
  }, [point, data, method]);

  return response;
}
