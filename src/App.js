import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter as Router } from "react-router-dom";
import "ui-components/dist/index.css";
import "./App.css";
import Header from "./component/Header";
import Search from "./component/Search";
import Routes from "./routing";

function App() {
  const {headerReducer} = useSelector(state => state)
  const [visible, setVisible] = useState(false)
  // const [getStateHeader, setStateHeader] = useState(true);
  useEffect(()=>{
    setVisible(headerReducer)
  }, [headerReducer])
  
  return (
    <Router>
      {visible && <Header />}
      {visible && <Search title="holis"/>}
      <Routes />
    </Router>
  );
}

export default App;
