import { createSlice } from '@reduxjs/toolkit';

const UtilitiesReducers = createSlice({
    name: 'utilities',
    initialState: {dataTitle: 'holis'},
    reducers: {
        setTitle(state, action) {
            state.dataTitle = action.payload
        }
    }
})

const {setTitle} = UtilitiesReducers.actions
const utilitiesReducers = UtilitiesReducers.reducer

export { setTitle, utilitiesReducers };
