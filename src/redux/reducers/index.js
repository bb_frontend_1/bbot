import { combineReducers } from "@reduxjs/toolkit";
import { dataApiReducer, setDataApi, setDataNav } from './dataApi';
import { headerReducer, isVisible } from './header';
import { setTitle, utilitiesReducers } from './utilities';

export { headerReducer, isVisible };
export { utilitiesReducers, setTitle };
export { dataApiReducer, setDataApi, setDataNav };

export default combineReducers({
  headerReducer,
  dataApiReducer,
  utilitiesReducers
})

