import { createSlice } from '@reduxjs/toolkit'

const dataApi = createSlice({
    name: 'dataApi',
    initialState: {dataNav: [], dataApi: []},
    reducers: {
        setDataApi(state, action) {
            state.dataApi = action.payload
        },
        setDataNav(state, action){
            state.dataNav = action.payload
        }
    }
})

const { setDataApi, setDataNav } = dataApi.actions
const dataApiReducer = dataApi.reducer


export { dataApiReducer, setDataApi, setDataNav }
