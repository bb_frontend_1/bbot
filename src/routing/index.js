import { lazy, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Route, Switch, useLocation } from 'react-router-dom';
import { isVisible } from '../redux/reducers';


const LoginPage = lazy(() => import('../pages/login'))
const UserRatings = lazy(() => import('../pages/User/UserRatings'))
const CreateUser = lazy(() => import('../pages/User/CreateUser'))


function usePageViews() {
    let location = useLocation();
    const dispatch = useDispatch()
    useEffect(() => {
      location.pathname === '/' ? dispatch(isVisible(false))  : dispatch(isVisible(true))
    })
  }

export default function Routes() {
    usePageViews();
    return (
        <Switch>
            <Route exact path="/" component={LoginPage}/>
            <Route exact path="/user" component={UserRatings}/>
            <Route exact path="/create-user" component={CreateUser}/>
        </Switch>
      )
}