import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Table } from 'ui-components';
import Wrapper from '../../../component/Wrapper';
import { request } from "../../../hooks";
import { setDataApi, setTitle } from '../../../redux/reducers';
import { UserRatingStyle } from './style';



function UserRatings() {
    const [getdataTable, setDataTable] = useState([])
    const dispatch = useDispatch()
    const data = request('getUsers','GET')
    const {dataApiReducer} = useSelector(state =>state)
    useEffect(() => {
        dispatch(setDataApi(data))
        dispatch(setTitle('Users ratings'))
        setDataTable(dataApiReducer.dataApi)
    }, [data, dispatch, dataApiReducer.dataApi])

    return(
        <Wrapper>
            <Row flex='true' justify-content="center">
                
                <UserRatingStyle>
                    <Table
                        headLabels={['Name', 'Last name', 'Email', 'Reading','Listening','Speaking','Writing','Restart Test']}
                        bodyContent={ getdataTable.length > 0 ? getdataTable.map((item, index) => (
                            [
                                {
                                    campoType: 'default',
                                    label: item.name.first
                                },
                                {
                                    campoType: 'default',
                                    label: item.name.last
                                },
                                {
                                    campoType: 'default',
                                    label: item.email
                                },
                                {
                                    campoType: 'default',
                                    label: `reading ${index}`
                                },
                                {
                                    campoType: 'default',
                                    label: `Listenig ${index}`
                                },
                                {
                                    campoType: 'default',
                                    label: `speaking ${index}`
                                },
                                {
                                    campoType: 'default',
                                    label: `writing ${index}`
                                },
                                {
                                    campoType: 'default',
                                    label: `Restart Test ${index}`
                                }
                            ]
                        )) : []
                    }
                    />
                </UserRatingStyle>
            </Row>
        </Wrapper>
    )
}

export default UserRatings