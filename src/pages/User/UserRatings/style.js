import styled from 'styled-components'

export const UserRatingStyle = styled.div`
    padding: 2rem;
    .ui-table-content{
        box-shadow: rgba(0, 0, 0, 0.23) 11px 10px 5px -5px;
        background-color: white;
        &-item{
            padding: .2rem 1rem;
        }
    }
    .ui-body-content-item{
        background-color: var(--primaryColor);
    }
`