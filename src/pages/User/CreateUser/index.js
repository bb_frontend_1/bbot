import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setTitle } from '../../../redux/reducers';
import { CreateUserStyle } from './style';


function CreateUser() {
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(setTitle('Create user'))
    }, [dispatch])

    return(
        <CreateUserStyle>
            <h1> Holis </h1>
        </CreateUserStyle>
    )
}

export default CreateUser;