import { useHistory } from "react-router-dom";
import { Button, Col, InputField, Row } from "ui-components";

export default function Login() {
  const history = useHistory();

  function handleClick() {
    history.push("/user");
  }

  return (
    <Row flex="true" justify-content="center" align-content="center">
      <Col xs="12" sm="8" md="6">
        <Row flex="true" justify-content="center">
          <img src="./assets/images/logo.svg" alt="logo BBOT" />
        </Row>
      </Col>
      <Col xs="12" sm="8" md="6">
        <Row flex="true" justify-content="center">
          <InputField label="Username" type="text" />
        </Row>
      </Col>
      <Col xs="12" sm="8" md="6">
        <Row flex="true" justify-content="center">
          <InputField label="Password" type="password" />
        </Row>
      </Col>
      <Col xs="12" sm="8" md="6">
        <Row flex="true" justify-content="center">
          <Button styled="primary" label="login" onClick={handleClick} />
        </Row>
      </Col>
    </Row>
  );
}
