import { Link } from "react-router-dom";
import { Icon } from "ui-components";
import Nav from "../Nav";
import Wrapper from "../Wrapper";
import { HeaderStyle } from "./styled";

function Header() {
  return (
    <HeaderStyle className="o-header">
      <div className="c-contHeader">
        <div className="c-contLogo">
          <img
            src="./assets/images/logo.svg"
            alt="Logo BBOT"
            style={{ width: "250px" }}
          />
        </div>
        <div className="c-contLink">
          <div className="c-contLink-item">
            <Icon nameIcon="person" />
            <Link to="/"> admindigital </Link>
          </div>
          <div className="c-contLink-item">
            <Icon nameIcon="exit_to_app" />
            <Link to="/" >
              Sign out
            </Link>
          </div>
        </div>
      </div>
      <Wrapper>
        <Nav />
      </Wrapper>
    </HeaderStyle>
  );
}

export default Header;
