import styled from "styled-components";

export const HeaderStyle = styled.header`
    
    .c-contHeader{
        display: grid;
        grid-template-columns: 1fr 0.8fr;
        align-items: center;
        justify-content: center;
        padding: 2rem 3rem
    }
    .c-contLogo, .c-contLink{
        display: flex;
        justify-content: flex-end;
        align-items: center;
        padding: 2rem 0;
    }

    .c-contLink-item{
        padding: 1rem;
        display: flex;
        align-items: center;
        
    }
    .c-contLink-item a{
        text-decoration: none;
        color: inherit;
    }
    

`