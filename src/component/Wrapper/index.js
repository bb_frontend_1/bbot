import { WrapperStyle } from "./styled";

function Wrapper(props) {
    return <WrapperStyle {...props}></WrapperStyle>
}

export default Wrapper