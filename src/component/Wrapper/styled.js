import styled from "styled-components";

export const WrapperStyle = styled.div`
        width: 100%;
        max-width: 1100px;
        margin: 0 auto;
        &:after{
            content: '';
            width: '300px';
            height:'200px';
        }
    `