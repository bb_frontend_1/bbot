import styled from "styled-components";

export const SearchStyle = styled.div`
    .SearchCont{
        background-color: white;
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        padding: 0 1rem;
        align-items: center;
        h4{
            grid-column: 2 / 3;
        }
    }
    .SearchItem{
        width: 100%;
        max-width: 300px;
    }

`