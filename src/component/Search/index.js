import { InputField, Text } from "ui-components";
import Store from '../../redux';
import { SearchStyle } from './styled';

function Search ({title}) {
    const {utilitiesReducers} = Store.getState('utilities/setTitle')
    
    return(
        <SearchStyle>
            <div className="SearchCont">
                <Text type="h4" text-align="center">
                    {utilitiesReducers.dataTitle}
                </Text>
                <InputField label='Search' type='text' dataIcon={{ state: true, nameIcon: 'search' }} addClass="SearchItem" />
            </div>
        </SearchStyle>
    )
}

export default Search;