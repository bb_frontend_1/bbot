import styled from "styled-components";

export const NavStyle = styled.nav`
    .c-contNav{
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        gap: 3rem;
    }
    .dropCont{
        border-radius: 10px;
        &:hover{
            background-color: white
        }
    }
    .dropItem{
        justify-content: center;
        grid-template-columns: 20px auto 20px;
        div[aria-labelledby="navbarDropdown"]{
            width: 100%;
            border-radius: 10px;
            background-color: var(--primaryColorDark);
            padding: 0;
            & > div {
            color: white;
            margin: 8px 5px;
            border-radius: 10px;
            position: relative;
            justify-content: flex-start;
                &::after{
                    content: '';
                    width: 80%;
                    height: 1px;
                    background-color: white;
                    position: absolute;
                    bottom: -4px;
                    margin-left: 10%;

                }
                &:hover{
                    background-color: white;
                    color: var(--primaryColorDark);
                    
                }
                &:last-child::after{
                    content: none;
                }
                & > div{
                    margin-left: 10%;
                }
            }
        } 
    }
`