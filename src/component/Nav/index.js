import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Dropdown, Icon } from "ui-components";
import { request } from "../../hooks";
import { setDataNav } from "../../redux/reducers";
import { NavStyle } from "./styled";

function Nav() {
  const [Data, SetData] = useState([]);
  const dispatch = useDispatch();
  const response = request("getNav", "GET");
  const { dataApiReducer } = useSelector((state) => state);

  useEffect(() => {
    dispatch(setDataNav(response));
    SetData(dataApiReducer.dataNav);
  }, [response, dispatch, dataApiReducer.dataNav]);

  return (
    <NavStyle>
      <div className="c-contNav">
        {Data.length > 0 &&
          Data.map((elem, idx) => (
            <div key={elem.id} className="dropCont">
              <Dropdown
                icon={elem.title.icon}
                label={elem.title.label}
                addClass="dropItem"
              >
                {elem.items.map((item) => (
                  <div key={item.id}>
                    <Icon nameIcon={item.icon} />
                    <Link
                      to= {item.link}
                    >
                       {item.label}
                    </Link>
                  </div>
                ))}
              </Dropdown>
            </div>
          ))}
      </div>
    </NavStyle>
  );
}

export default Nav;
